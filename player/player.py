import sounddevice as sd
import numpy as np
from logmmse import logmmse
import os
import torch

device = torch.device('cpu')
torch.set_num_threads(4)
local_file = 'model.pt'

if not os.path.isfile(local_file):
    torch.hub.download_url_to_file('https://models.silero.ai/models/tts/ru/v2_baya.pt',
                                   local_file)  

model = torch.package.PackageImporter(local_file).load_pickle("tts_models", "model")
model.to(device)

sample_rate = 16000

def speak(text):
    audio = model.apply_tts(texts=[text],
                  sample_rate=sample_rate)
    enhanced = logmmse(np.array(audio[0]), sample_rate, output_file=None, initial_noise=1, window_size=160, noise_threshold=0.15)
    sd.play(enhanced, sample_rate)
    sd.wait()

print("Player is ready...")

speak("Привет! Я Пятница!")